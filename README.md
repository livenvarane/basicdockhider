# BasicDockHider #

Hide the dock background or change its opacity

### Compatible devices/versions ###

* iOS 10.x-13.x (not iPadOS)
* Tested on checkra1n 13.3.1 (must work with unc0ver)

### Problems ? Glitchs ? ###

Contact me:

* @LivenOff (Twitter)
* u/LVN_N (Reddit)